# Social network
This program repeats the main functionality of the social network. Users can:
* Register and log in
* Create a profile
* Add friends
* Publish posts
* Comment on posts
* Communicate through messenger

### Frontend:
* React.js
* MobX
* Socket.IO
* REST API
* Axios

### Backend:
* Node.js
* MySQL
* Express
* Socket.IO

## Start the program:
* Install MySQL on the local machine.
* Add database configuration to `.env` file in backend.
* Run the `npm install` command in both directories (client, server).
* Execute the `npm start` command in both directories (client, server).