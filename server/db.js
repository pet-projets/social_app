const {Sequelize} = require('sequelize')

module.exports = new Sequelize(
    'socialapp',
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
        dialect: 'mysql',
        host: process.env.DB_HOST,
        port: process.env.DB_PORT
    }
)